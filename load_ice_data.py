import pandas as pd

def get_ice_data():
    data = pd.read_excel("data/Sea_Ice_Index_Daily_Extent_G02135_v3.0.xlsx")

    # Extract the keys for the yearly data 
    # 0, 1 - month, day of the year
    # last 3 - empty, mean, median
    av_years = list(data.keys())[2:-3]

    # rename columns to clearly name the content
    new_names = {'Unnamed: 0':'Month', 'Unnamed: 1':'DayOfMonth'}
    data.rename(columns=new_names, inplace=True)

    # The first unlabeld data column only contains the month for the first day of each month
    # first step is replacing the nans for each month with the name of the month
    # to enable searching and sorting based on the month

    data['Month'] = data['Month'].fillna(method='ffill')
    # ffill replaces the nans with the last value in the data, bfill with the next value

    # The first few years contain missing data points
    # Might affect yearly averages, especally the amount of data missing in year 1
    print('The data rows are: Month, Day of the Year followed by the specific years from' 
          + ' 1978 to 2021')
    print("The data contains gaps before 1989")
    print("The data is the extend of the arctic sea ice in million square meters")
    
    # Delete Additional Data columns
    del data[" "]
    del data["1981-2010 mean"]
    del data["1981-2010 median"]
    
    # print(data.keys())
    
    return data
