import load_ice_data as ds
import numpy as np
import matplotlib.pyplot as plt

from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

data = ds.get_ice_data()

# Calculate Min, Max and Mean of the complete data range
np_data = data.drop(columns=["Month", "DayOfMonth"]).to_numpy()
a_min = np.nanmin(np_data)
a_max = np.nanmax(np_data)
a_mean = np.nanmean(np_data)

print("All Time")
print("Min : " + str(a_min) )
print("Max : " + str(a_max) )
print("Mean : " + str(a_mean) )

# Calculate the Yearly Min, Max and Mean Values
years = np.array(range(1978, 2022))

y_min = data.drop(columns=["Month", "DayOfMonth"]).min().to_numpy()
y_max = data.drop(columns=["Month", "DayOfMonth"]).max().to_numpy()
y_mean = data.drop(columns=["Month", "DayOfMonth"]).mean().to_numpy()


# Regression Linear
years_pred = np.array([range(1978, 2067)]).reshape(-1, 1)

years = np.array([years]).reshape(-1, 1)
y_min = np.array([y_min]).reshape(-1,1)
y_max = np.array([y_max]).reshape(-1,1)
y_mean = np.array([y_mean]).reshape(-1,1)

regr_min = linear_model.LinearRegression()
regr_max = linear_model.LinearRegression()
regr_mean = linear_model.LinearRegression()
regr_min.fit(years, y_min)
regr_max.fit(years, y_max)
regr_mean.fit(years, y_mean)

ice_pred_min = regr_min.predict(years_pred)
ice_pred_max = regr_max.predict(years_pred)
ice_pred_mean = regr_mean.predict(years_pred)


scatter_size = 10

plt.figure(1)
plt.scatter(years, y_min, label="min", s=scatter_size)
plt.scatter(years, y_max, label="max", s=scatter_size)
plt.scatter(years, y_mean, label="mean", s=scatter_size)
plt.plot(years_pred, ice_pred_min, '--', label="reg min")
plt.plot(years_pred, ice_pred_max, '--', label="reg max")
plt.plot(years_pred, ice_pred_mean, '--', label="reg mean")
plt.title("Arctic Sea Ice over the Years")
plt.xlabel("Year")
plt.ylabel("Arctic Sea Ice (Million Square Meters)")
plt.legend()
plt.grid()
plt.show()