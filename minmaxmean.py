import load_ice_data as ds
import numpy as np
import matplotlib.pyplot as plt

data = ds.get_ice_data()

# Calculate Min, Max and Mean of the complete data range
np_data = data.drop(columns=["Month", "DayOfMonth"]).to_numpy()
a_min = np.nanmin(np_data)
a_max = np.nanmax(np_data)
a_mean = np.nanmean(np_data)

print("All Time")
print("Min : " + str(a_min) )
print("Max : " + str(a_max) )
print("Mean : " + str(a_mean) )

# Calculate the Yearly Min, Max and Mean Values
years = range(1978, 2022)

y_min = data.drop(columns=["Month", "DayOfMonth"]).min().to_numpy()
y_max = data.drop(columns=["Month", "DayOfMonth"]).max().to_numpy()
y_mean = data.drop(columns=["Month", "DayOfMonth"]).mean().to_numpy()

print("Yearly Data")
print("Min : " + str(y_min) )
print("Max : " + str(y_max) )
print("Mean : " + str(y_mean) )


plt.figure(1)
plt.plot(years, y_min, label="min")
plt.plot(years, y_max, label="max")
plt.plot(years, y_mean, label="mean")
plt.title("Arctic Sea Ice over the Years")
plt.xlabel("Year")
plt.ylabel("Arctic Sea Ice (Million Square Meters)")
plt.legend()
plt.grid()
plt.show()